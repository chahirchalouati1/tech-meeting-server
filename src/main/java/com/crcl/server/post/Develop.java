package com.crcl.server.post;

import com.crcl.server.post.domain.Post;
import com.crcl.server.post.repository.PostRepository;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
public class Develop {
    private final Faker faker = new Faker();
    @Autowired
    PostRepository postRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void addData() {
        postRepository.deleteAll().subscribe();
        for (int i = 0; i < 100; i++) {
            final Post post = new Post(
                    faker.book().title(),
                    faker.lorem().paragraph(),
                    "https://picsum.photos/800/600?random=1");


            postRepository.insert(post)
                    .subscribe();
        }

    }

}
